package com.atlassianlabs.deployscripts;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.EvaluateXPathMatcher.hasXPath;
import static org.xmlunit.matchers.HasXPathMatcher.hasXPath;
import static org.hamcrest.CoreMatchers.*;

public class XmlPurgeTest
{
    @Test
    public void test()
    throws Exception
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (InputStream is = XmlPurgeTest.class.getResourceAsStream("/test1.xml"))
        {
            XmlPurge.runWithStreams(is, os);
        }

        String xml = os.toString();
        System.out.println(xml);
        assertThat(xml, hasXPath("/entity-engine-xml"));
        assertThat(xml, hasXPath("count(//Action)", is("2")));
        assertThat(xml, hasXPath("count(//LicenseRoleDefault)", is("1")));
        assertThat(xml, hasXPath("count(//KPlugins)", is("0")));
        assertThat(xml, hasXPath("count(//KPluginsCfg)", is("0")));

    }
}
