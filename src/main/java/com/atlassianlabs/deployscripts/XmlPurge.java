package com.atlassianlabs.deployscripts;

import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.stream.EventFilter;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.XMLEvent;

/**
 * This class provides XML purging functionality for JIRA entities.xml to avoid or fix https://confluence.atlassian.com/x/CY3zEg.
 * It uses Woodstox so we get <x/> instead of <x></x>, otherwise the output can end up larger than the input.
 * Originally written by David Currie & Peter Runge (pretty much all Peter, let's be honest).
 */
public class XmlPurge
{
    /*
    Takes a file as an argument and then outputs the results to stdout. Best used with java -jar xml-purge filename > filename-purged.
     */
    public static void main(String... args)
    throws Exception
    {
        if (args.length == 0)
        {
            System.out.println("Please specify the path to the file to purge.");
            System.exit(0);
        }
        Path xmlFile = Paths.get(args[0]);
        try (InputStream is = Files.newInputStream(xmlFile))
        {
            runWithStreams(is, System.out);
        }
    }

    // This exists so the method can be run to output to memory, so we can run unit tests on it.
    public static void runWithStreams(InputStream is, OutputStream os)
    throws Exception
    {
        WstxInputFactory wstxInputFactory = new WstxInputFactory();
        EventFilter filter = new RemoveElementsFilter();
        XMLEventReader reader = wstxInputFactory.createFilteredReader(wstxInputFactory.createXMLEventReader(is), filter);
        try
        {
            WstxOutputFactory wstxOutputFactory = new WstxOutputFactory();
            wstxOutputFactory.setProperty(WstxOutputFactory.P_AUTOMATIC_EMPTY_ELEMENTS, true);
            XMLEventWriter writer = wstxOutputFactory.createXMLEventWriter(os);
            try
            {
                writer.add(reader);
            }
            finally
            {
                writer.close();
            }
        }
        finally
        {
            reader.close();
        }
    }

    /**
     * This set is the static list of known invalid tables to remove. They are considered invalid as they implement OfBiz
     * rather than Active Objects and as such the XML import will fail unless the plugin is installed and enabled.
     */
    private static final Set<String> PURGED_NAMES = new HashSet<>(Arrays.asList(
            "TimesheetEntity", "TimesheetScheme", "Myaa", "ReportEntity", "ReportScheme", "LinkEntity",
            "LinkScheme", "WorklogExt", "WorklogType", "KPlugins", "KPluginsCfg", "cfgDefaultValue",
            "cfgLDefaultValue", "cfgValue", "cfgLValue", "QANDAA", "QANDAQ", "QANDAE", "a_text", "q_text",
            "jjlf_project", "jjlf_config", "jjlf_category", "KListenerSils", "JiraCapacityPlan",
            "additionalTaskDescription", "testexecutionhistory", "KRSSecurity", "KBlitzActions"
    ));

    // Should we purge the provided QName? Depends what's in the set.
    private static boolean purgedName(QName name)
    {
        return PURGED_NAMES.contains(name.getLocalPart());
    }

    // Filter used to remove the appropriate XML elements.
    public static class RemoveElementsFilter implements EventFilter
    {
        @Override
        public boolean accept(final XMLEvent event)
        {
            if (event.isStartElement() && purgedName(event.asStartElement().getName()))
                return false;
            else if (event.isEndElement() && purgedName(event.asEndElement().getName()))
                return false;
            else
                return true;
        }
    }
}
