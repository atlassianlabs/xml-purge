Execute it against an entities XML file to get purged output to standard out. Removes known XML data that will prevent an import, namely those detailed in https://confluence.atlassian.com/x/CY3zEg.

This is separate to https://confluence.atlassian.com/display/JIRA/Removing+invalid+characters+from+XML+backups as it removes certain table (plugins that use OfBiz instead of Active Objects) data as opposed to invalid XML characters.

To run:
```
mvn package
java -jar target/xml-purge-1.0-SNAPSHOT.jar /path/to/entities.xml > /path/to/entities-purged.xml
```